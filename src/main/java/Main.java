import pl.sda.myLinkedList.MyLinkedList;
import pl.sda.myLinkedList.MyNode;

public class Main {
    public static void main(String[] args) {


        MyNode<String> myNode = new MyNode<>("my first node");
//        myNode.setData("my first node");


        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.addAtEnd(myNode);
        System.out.println(myLinkedList.getFirst());
        myLinkedList.getNElement(1);


        MyNode<Integer> node0 = new MyNode<>(0);
        MyNode<Integer> node1 = new MyNode<>(1);
        MyNode<Integer> node2 = new MyNode<>(2);
        MyNode<Integer> node3 = new MyNode<>(3);
        MyNode<Integer> node4 = new MyNode<>(4);

        MyLinkedList myLinkedListInteger = new MyLinkedList();
        myLinkedListInteger.addAtEnd(node0);
        myLinkedListInteger.addAtEnd(node1);
        myLinkedListInteger.addAtEnd(node2);
        myLinkedListInteger.addAtEnd(node3);
        myLinkedListInteger.addAtEnd(node4);

        System.out.println(myLinkedListInteger.getFirst());
        System.out.println(myLinkedListInteger.getNElement(0));
        System.out.println(myLinkedListInteger.getNElement(1));
        System.out.println(myLinkedListInteger.getNElement(2));
        System.out.println(myLinkedListInteger.getNElement(3));
        System.out.println(myLinkedListInteger.getNElement(4));


    }
}

package pl.sda.myLinkedList;

public class MyNode <T> {

    private T data;
    public MyNode<T> next;


    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public MyNode<T> getNext() {
        return next;
    }

    public void setNext(MyNode<T> next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return data.toString();
    }

    public MyNode(T data) {
        this.data = data;
    }
}

package pl.sda.myLinkedList;

public class MyLinkedList<T> {

    private MyNode<T> head = null;

    // Wstawianie na koniec
    public void addAtEnd(T data) {

        MyNode newNode = new MyNode<>(data);
//        newNode.setData(data);

        if (head == null) {
            head = newNode;
        } else {


            MyNode<T> last = head;
            while (last.getNext() != null) {
                last = last.getNext();
            }
            last.setNext(newNode);
        }
    }


    // Odczytanie pierwszego elementu listy
    public MyNode<T> getFirst() {
        return head;
    }


    // Odczytanie dowolnego elementu z listy (jako parametr przyjmij indeks elementu)
    public MyNode<T> getNElement(int i) {
        MyNode<T> current = head;

        for (int j = 0; j < i; j++) {
            current = current.getNext();
        }
        return current;
    }


    // Dodanie elementu z przodu listy(jako pierwszy element)
    public void addAsFirst(T data) {
        MyNode newNode = new MyNode<>(data);
//        newNode.setData(data);

        newNode.setNext(head);
        head = newNode;

    }


    // Usunięcie elementu (jako parametr przyjmij indeks elementu)
    public void deleteAtIndex(int i) {

        MyNode<T> previous = head;

        for (int j = 0; j < i-1; j++) {
            previous = previous.getNext();
        }

        MyNode<T> current = previous.getNext();
        MyNode<T> next = current.getNext();

        previous.setNext(next);
        current = null;

    }



    // Dodanie elementu w „środek” listy, pomiędzy innymi (jako parametr przyjmij indeks elementu za którym nastąpi wstawienie nowego)


    public void insertAtIndex(int i, T data) {
        MyNode<T> toInsert = new MyNode(data);
        MyNode<T> previous = head;

        for (int j = 0; j < i-1; j++) {
            previous = previous.getNext();
        }

        MyNode<T> current = previous.getNext();
        MyNode<T> next = current.getNext();

        previous.setNext(toInsert);
        toInsert.setNext(next);
    }


}
